// Methods related to links

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Events } from './models.js';
import { Members } from './models.js';
import { Points } from './models.js';
import { Meetings } from './models.js';
import { Interests } from './models.js';


Meteor.methods({
  'events.insert'(event, members, points) {
    //check(url, String);
    //check(title, String);
    var eventId = Events.insert(event);
    for (var i = 0; i < members.length; i++) {
      Members.insert({ eventId: eventId, userId: members[i] });
    }
    for (var i = 0; i < points.length; i++) {
      points[i].eventId = eventId;
      Points.insert(points[i]);
    }
    return eventId;
  },
  'member.insert'(eventId, user) {
    if (typeof user !== 'string') { user = userId; }
    return Members.insert({ eventId: eventId, userId: user });
  },
  'point.insert'(eventId, point) {
    point.eventId = eventId;
    return Points.insert(point);
  },
  'interest.insert'(interest) {
    return Interests.insert(interest)
  },
  'meetings.check'(eventId, startPoint, endPoint) {

  }
});
