// Definition of the links collection

import { Mongo } from 'meteor/mongo';

export const Events = new Mongo.Collection('events');

export const Members = new Mongo.Collection('members');

export const Interests = new Mongo.Collection('interests');

export const Points = new Mongo.Collection('points');

export const Meetings = new Mongo.Collection('meetings');
