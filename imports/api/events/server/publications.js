// All links-related publications

import { Meteor } from 'meteor/meteor';
import { Events } from '../models.js';
import { Members } from '../models.js';
import { Points } from '../models.js';
import { Interests } from '../models.js';
import { Meetings } from '../models.js';

Meteor.publish('events.all', function () {
  return Events.find();
});

Meteor.publish('users.all', function () {
  return Meteor.users.find();
});

Meteor.publish('members.all', function () {
  return Members.find();
});

Meteor.publish('points.all', function () {
  return Points.find();
});

Meteor.publish('interests.all', function () {
  return Interests.find();
});

Meteor.publish('meetings.all', function () {
  return Meetings.find();
});
